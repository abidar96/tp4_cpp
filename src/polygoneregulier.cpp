#include "polygoneregulier.h"
#include <math.h>
#define PI 3.141592
#include<iostream>
using namespace std;


Polygoneregulier::Polygoneregulier(const Couleur& couleur,const point& centre,int rayon,int nbCotes)
:FigureGeometrique(couleur)
{
    _nbPoints=nbCotes;
    _points = new point [nbCotes];
    for(int i=0;i<nbCotes;i++){
    point tmp;

    float  ALPHA = i * PI * 2 / nbCotes;
        tmp._x = rayon * cos(ALPHA) + centre._x;
        tmp._y = rayon * sin(ALPHA) + centre._y;
        _points[i]=tmp;

    }


}

void Polygoneregulier::Afficher() const
{
    cout << "PolygoneRegulier " <<_couleur._r<<"_"<<_couleur._g <<"_" << _couleur._b ;
    //cout <<" "<< _points[0]._x <<"_" <<_points[0]._y;
     for(int i=0;i<_nbPoints;i++){
          cout <<" "<< _points[i]._x <<"_" <<_points[i]._y;
     }
}
int Polygoneregulier::getNbPoints() const {
        return _nbPoints;
}

const point& Polygoneregulier::getPoint(int indice) const{

    return _points[indice];

}
