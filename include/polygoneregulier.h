#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H
#include "point.h"
#include "figuregeometrique.h"

class Polygoneregulier : public FigureGeometrique
{
    public:
        Polygoneregulier(const Couleur& couleur,const point& centre,int rayon,int nbCotes);
        virtual void Afficher() const;
        int getNbPoints() const;
        const point& getPoint(int indice) const;
    protected:

    private:
       int _nbPoints;
       point* _points;
};

#endif // POLYGONEREGULIER_H
