#ifndef FIGUREGEOMETRIQUE_H
#define FIGUREGEOMETRIQUE_H
#include"Couleur.h"
#include"Point.h"



class FigureGeometrique
{
    protected:
        Couleur _couleur;
    public:
        FigureGeometrique( const Couleur& couleur);
        const Couleur& getCouleur() const;
        virtual void Afficher() const;


};

#endif // FIGUREGEOMETRIQUE_H
